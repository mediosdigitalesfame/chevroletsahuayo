<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Financiamiento Chevrolet</title>
	<?php include('contenido/head.php'); ?>
</head>
<body><em></em>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    <?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Financiamiento</h2>

				</div>
			</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">
                        
                        <div class="col-md-6" align="center">
                                 <div class="container">
					                 <div class="col-md-12" >
								         <?php include('form.php'); ?>
                                     </div>
                                 </div>
                             </div>
                        
						<div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li><span><i class="fa fa-home"></i>Blvd. Lázaro Cárdenas S/N</span> <span>C.P. 59000</span> <span>Sahuayo, Michoacán</span></li>
									<li><span><i class="fa fa-phone"></i><strong>(353) 532 0378</strong></span></li>
<!--                                    <li><i class="fa fa-phone"></i><span>Taller de Servicio <strong>Ext. 106</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Postventa <strong>Ext. 106</strong></span><br>
									<i class="fa fa-phone"></i><span>Refacciones <strong>Ext. 106</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Ventas <strong>Ext. 108</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Seminuevos <strong>Ext. 112</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Recepción <strong>Ext. 101</strong></span><br>                

                                    <i class="fa fa-phone"></i><span>Financiamiento <strong>Ext. 114</strong></span><br>  
                                    --><h3>Whatsapp</h3>
                               <li><span><i class="fa fa-whatsapp"></i><strong>  Ventas   | 3531050482 </strong></span></li><strong></strong> 

                               <h3>Whatsapp</h3>
                               <li><span><i class="fa fa-whatsapp"></i><strong>  Citas de Servicio <br>4432732794 | 4433771717</strong></span></li>
                               
		</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>Chevrolet FAME</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>
								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>
							</div>
						</div>
                
					</div>
				</div>
			</div>

		</div> 

		<br>
        
		<?php include('contenido/footer.php'); ?>
     </div> 			
	
</body>
</html>